import { Component, OnInit, ViewChild, TemplateRef, ElementRef, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

/**
* Modules
*/
import { environment } from 'src/environments/environment';
import { GlobalService } from 'src/core/services/global/global.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  confirmed: boolean;
  emailHide: boolean;
  email: string;

  constructor(
    private cookieService: CookieService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
    private fb: FormBuilder
  ) { 
    this.confirmed = false
    this.emailHide = false
  }

  ngOnInit(): void {
    if (this.hasParamToken()) {
      this.checkToken(this.activatedRoute.snapshot.params.token.split('/').join('%2F'));
    } else {
      alert('Unauthorized Token...');
      this.router.navigateByUrl('/login');
    }
  }

  hasParamToken(): Boolean {
    return !!this.activatedRoute.snapshot.params.token ? true : false;
  }

  checkToken(token: string) {

    this.globalService.confirm(token).subscribe(res => {
      if (res.status == 200) {
          const data = res.data
          if (!res.data.code) {
            alert('Mismatch in Token...');
            this.router.navigateByUrl('/login');
          }
          this.globalService.clientLogin(data).subscribe(res2 => {
          if (res2.status == 200) {
            alert('Login correct, redirecting to the SOM keychain.')
            const token = res2.token;
            this.cookieService.set(environment.cookie, JSON.stringify(token), 0, '/', `${location.hostname}`, false, 'Lax');
            this.globalService.setUserData(res2.user);
            this.router.navigateByUrl(`/som/keychain`);
          }
          if (res2.status == 401) {
            alert(res2.msg)
          }
        }, error => {
          alert('Error - The invitation has not been sent to the email written')
        });
      } else {
        alert(res.msg || 'Token error.');
        this.router.navigateByUrl('/login');
      }
    }, error => {
      alert(error.error.msg || 'Unauthorized - This route may have expired already...');
      this.router.navigateByUrl('/login');
    });
  }

}


