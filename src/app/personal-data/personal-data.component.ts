import { Component, OnInit, ViewChild, TemplateRef, ElementRef, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

/**
* Modules
*/
import { GlobalService } from 'src/core/services/global/global.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css']
})
export class PersonalDataComponent implements OnInit {

  email: string;

  constructor(
    private cookieService: CookieService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    if (this.hasParamToken()) {
      this.checkToken(this.activatedRoute.snapshot.params.token.split('/').join('%2F'));
    } else {
      alert('Unauthorized Token...');
      this.router.navigateByUrl('/login');
    }
  }

  hasParamToken(): Boolean {
    return !!this.activatedRoute.snapshot.params.token ? true : false;
  }

  checkToken(token: string) {

    this.globalService.confirm(token).subscribe(res => {
      if (res.status == 200) {
          const data = res.data
          if (data.code) {
            alert('Mismatch in Token...');
            this.router.navigateByUrl('/login');
          }
          this.email = data.email
      } else {
        alert(res.msg || 'Token error.');
        this.router.navigateByUrl('/login');
      }
    }, error => {
      alert(error.error.msg || 'Unauthorized - This route may have expired already...');
      this.router.navigateByUrl('/login');
    });
  }
}
