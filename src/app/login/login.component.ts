import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

/**
* Modules
*/
import { environment } from 'src/environments/environment';
import { GlobalService } from 'src/core/services/global/global.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailFormGroup: FormGroup;
  dataFormGroup: FormGroup;
  invited: boolean;
  data: boolean;
  @Input() email: string;

  constructor(
    private fb: FormBuilder,
    private globalService: GlobalService,
    private cookieService: CookieService,
    private router: Router,
    ) { 
      this.invited = false;

      if (this.email) {
        this.data = true;
        this.dataFormGroup =  this.fb.group({
          email: [{disabled: true, value: this.email}, [Validators.required, Validators.email]],
          name: ['', [Validators.required]],
          occupation: ['', [Validators.required]],
          phone: ['', [Validators.required, Validators.maxLength(15)]],
        });
      }
  }

  ngOnInit() {
    this.createLoginForms();
  }

  createLoginForms() {
    this.emailFormGroup =  this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });

    /*this.loginFormGroup =  this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });*/
  }

  sendInvitationCode(login = false, personal = false) {

    const verifiedFormGroup = personal ? this.dataFormGroup : this.emailFormGroup

    if (!verifiedFormGroup.valid){
      alert('Data entered is not complete or is not a valid email')
      return;
    }

    const data = verifiedFormGroup.getRawValue()
    if (!login) {
      data.personal = personal
      this.globalService.access(data).subscribe(res => {
        if (res.status == 200) {

          if (res.hasData){
            alert(res.msg || 'Verification code has been sent to the written email')
            this.invited = true;
            this.data = false;
            this.emailFormGroup =  this.fb.group({
              email: [{disabled: true, value: data.email}, [Validators.required, Validators.email]],
              code: ['', [Validators.required, Validators.maxLength(5)]],
            });
          } else {
            alert(res.msg || 'New Guest User registered successfully. Please fill your personal info.')
            this.data = true;
            this.dataFormGroup =  this.fb.group({
              email: [{disabled: true, value: data.email}, [Validators.required, Validators.email]],
              name: ['', [Validators.required]],
              occupation: ['', [Validators.required]],
              phone: ['', [Validators.required, Validators.maxLength(15)]],
            });
          }
        }
        if (res.status == 401) {
          alert(res.msg)
        }
      }, error => {
        alert(error.error.msg || 'Error - The invitation has not been sent to the email written')
      });
    } else {
      this.globalService.clientLogin(data).subscribe(res => {
        if (res.status == 200) {
          alert('Login correct, redirecting to the SOM keychain.')
          const token = res.token;
          this.cookieService.set(environment.cookie, JSON.stringify(token), 0, '/', `${location.hostname}`, false, 'Lax');
          this.getUserInfo(res.user)
          this.router.navigateByUrl('/som/keychain');
        }
        if (res.status == 401) {
          alert(res.msg)
        }
      }, error => {
        alert(error.error.msg || 'Error - The invitation has not been sent to the email written')
      });
    }
  }

  /*login(){
    if (!this.loginFormGroup.valid){
      alert('Data entered is not complete or is not a valid email')
      return;
    }
    this.globalService.login(this.loginFormGroup.value).subscribe((res) => {
      if (res.status == 200) {
        const token = res.token;
        this.cookieService.set(environment.cookie, JSON.stringify(token), 0, '/', `${location.hostname}`, false);
        this.getUserInfo(res.user)
        this.router.navigateByUrl('/auth/main');
      }
      if (res.status == 401) {
        alert(res.msg || 'Something went wrong, check console for more details.')
      }
    }, error => {
      alert(error.error.msg || 'Something went wrong, check console for more details.')
    })
  }*/

  getUserInfo(user) {
      this.globalService.setUserData(user);
  }

}
