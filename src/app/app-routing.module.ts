import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**
 * Components
 */

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { LoginComponent } from './login/login.component';
import { QrcodeComponent } from './qrcode/qrcode.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AuthGuard } from 'src/core/guards/auth.guard';
import { ConfirmationComponent } from './confirmation/confirmation.component';

let routes: Routes = [
  { path: 'confirm-code/:token', component: ConfirmationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'login/:token', component: PersonalDataComponent },
  { path: 'som/keychain', component: QrcodeComponent, canActivate: [AuthGuard] },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
