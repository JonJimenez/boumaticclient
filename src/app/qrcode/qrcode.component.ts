import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * Services
 */
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from 'src/core/services/global/global.service';

/**
 * Modules
 */
import { environment } from '../../environments/environment';
import Ws from '@adonisjs/websocket-client/index';

import QRious from 'qrious';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css']
})
export class QrcodeComponent implements OnInit {

  keychain: any;
  keychainKey: any;
  roles: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService,
    private globalService: GlobalService) {
      this.roles = {
        '1': 'Guest',
        '2': 'Owner',
        '3': 'Dealer',
        '4': 'Veterinary',
        '5': 'Operator'
      }
    }

  ngOnInit() {

    this.keychain = []

    this.getKeychain()
    
  }

  getKeychain() {
    this.globalService.getKeychain().subscribe(res => {
      if (res.status == 200) {
        this.keychainKey = res.data
        
        for (const key in this.keychainKey) {
          if (Object.prototype.hasOwnProperty.call(this.keychainKey, key)) {
            this.keychain.push(key)
          }
        }
      } else {
        alert(res.msg || 'Error en la revisión del Token.');
        this.router.navigateByUrl('/login');
      }
    }, error => {
      alert(error.error.msg || 'No autorizado...');
      this.router.navigateByUrl('/login');
    });
  }

  Logout() {
    this.globalService.logout().subscribe(res => {
      if (res.status == 200) {
          this.cookieService.delete(environment.cookie, '/', location.hostname);
          this.cookieService.delete('routes', '/', location.hostname);
          this.router.navigateByUrl('/login');
      }
    }, error => {
      alert(error.error.msg || 'No autorizado...');
      this.router.navigateByUrl('/login');
    });
  }

}
