import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';

import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(
        private cookieService: CookieService,
        private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.cookieService.check(environment.cookie)
            ? JSON.parse(this.cookieService.get(environment.cookie)).token
            : '';
        const reqClone = req.clone({ setHeaders: { 'Authorization': 'Bearer ' + token } });
        return next.handle(reqClone).pipe( tap(() => {},
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status !== 401) {
                        return;
                    }
                    this.cookieService.delete(environment.cookie, '/', location.hostname);
                    this.cookieService.delete('routes', '/', location.hostname);
                    this.router.navigateByUrl('/login');
                }
            }
        ));
    }
    
}