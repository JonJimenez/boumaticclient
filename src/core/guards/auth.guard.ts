import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { GlobalService } from '../services/global/global.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  isLoggedIn = false;
  redirectUrl: string;

  constructor(private router: Router,
    private cookieService: CookieService,
    private globalService: GlobalService) {

    }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    return this.checkLogin(state.url);
  }

  checkLogin(url: string) {
    if (this.cookieService.get(environment.cookie)) {
      if (this.isLoggedIn) {
        return true;
      } else {
        this.globalService.isLoggedIn().subscribe(res => {
          this.isLoggedIn = true;
          this.globalService.setUserData(res);
          this.router.navigateByUrl(url);
        }, err => {
          this.isLoggedIn = false;
          this.cookieService.delete(environment.cookie, '/', location.hostname);
          this.cookieService.delete('routes', '/', location.hostname);
          this.router.navigateByUrl('/login');
        });
      }
    } else {
      this.isLoggedIn = false;
      this.cookieService.delete(environment.cookie, '/', location.hostname);
      localStorage.removeItem('keychain')
      this.router.navigateByUrl('/login')
    }
  }
}
