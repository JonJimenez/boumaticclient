import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  url: string  = environment.authGlobal;
  token: string;
  headers: HttpHeaders;
  user: any;

  constructor(private http: HttpClient) { }

  setUserData(value: any) {
    this.user = value;
  }

  logout(): Observable<any> {
    return this.http.post<any>(`${this.url}/logout`, {});
  }

  isLoggedIn(): Observable<any> {
    return this.http.post<any>(`${this.url}/check/login`, {});
  }

  checkToken(token: string): Observable<any> {
    const data = {token: token};
    const url = `${this.url}/check/`;
    return this.http.post(url, data);
  }

  login(data: any): Observable<any> {
    return this.http.post<any>(`${ this.url }/login`, data);
  }

  clientLogin(data: any): Observable<any> {
    return this.http.post<any>(`${ this.url }/client-login`, data);
  }

  access(data: any): Observable<any> {
    return this.http.post<any>(`${ this.url }/access`, data);
  }

  confirm(token: string): Observable<any> {
    return this.http.get<any>(`${ this.url }/confirmUser/${token}`);
  }

  getAdminData(): Observable<any> {
    return this.http.post<any>(`${ this.url }/getAdminData`, {});
  }

  getKeychain(): Observable<any> {
    return this.http.post<any>(`${ this.url }/getKeychain`, {});
  }
}
